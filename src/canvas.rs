use cairo;
use gtk::DrawingArea;

use super::eqn;

#[derive(Clone)]
pub struct Graph {
    pub letter: char,
    pub string: String,
    rpn_equation: Vec<eqn::Token>,
    width: f64,
    color_r: f64,
    color_g: f64,
    color_b: f64
}

pub struct Canvas {
    pub area: DrawingArea,
    pub graphs: Vec<Graph>,
    min_x: f64,
    max_x: f64,
    min_y: f64,
    max_y: f64,
}

impl Canvas {
    pub fn new (area: DrawingArea) -> Canvas {
        Canvas {
            area,
            graphs: Vec::new(),
            min_x: -2.0,
            max_x: 2.0,
            min_y: -2.0,
            max_y: 2.0,
        }
    }

    pub fn add_graph (&mut self, letter: char, string: String, width: f64, color_r: f64, color_g: f64, color_b: f64) {
        self.graphs.push(Graph {
            letter,
            rpn_equation: eqn::infix_to_rpn(eqn::tokenize_str(&string)),
            string,
            width,
            color_r,
            color_g,
            color_b
        })
    }

    pub fn zoom_in (&mut self) {
        self.min_x *= 0.9;
        self.max_x *= 0.9;
        self.min_y *= 0.9;
        self.max_y *= 0.9;
    }

    pub fn zoom_out (&mut self) {
        self.min_x *= 1.1;
        self.max_x *= 1.1;
        self.min_y *= 1.1;
        self.max_y *= 1.1;
    }

    fn pix_x_to_coord_x (&self, x: f64, width: f64) -> f64 {
        (x*(self.max_x-self.min_x))/(width)+self.min_x
    }

    fn coord_x_to_pix_x (&self, x: f64, width: f64) -> f64 {
        ((x-self.min_x)*width)/(self.max_x-self.min_x)
    }

    fn coord_y_to_pix_y (&self, y: f64, height: f64) -> f64 {
        (-height*(y-self.min_y))/(self.max_y-self.min_y)+height
    }

    fn draw_graph (&self, c: &cairo::Context, width: f64, height: f64, func: Graph){
        c.set_line_width(func.width);
        c.set_source_rgb(func.color_r, func.color_g, func.color_b);

        c.move_to(-10.0, -10.0);

        for x in -1..width as isize {
            let x_val = x as f64;

            let result = eqn::solve_rpn(func.rpn_equation.clone().into_iter().map(|x| {
                if x == eqn::Token::Variable('x') {
                    eqn::Token::Number(self.pix_x_to_coord_x(x_val, width))
                } else {
                    x
                }
            }).collect::<Vec<eqn::Token>>());

            let y_val = self.coord_y_to_pix_y(result, height);

            c.line_to(x_val, y_val);
            c.move_to(x_val, y_val);
        }

        c.stroke();
    }

    fn draw_grid(&self, c: &cairo::Context, width: f64, height: f64) {
        c.set_line_width(1.0);
        c.set_source_rgb(0.0, 0.0, 0.0);

        let x_axis_pos = self.coord_y_to_pix_y(0.0, height);
        c.move_to(0.0, x_axis_pos);
        c.line_to(width, x_axis_pos);
        
        let y_axis_pos = self.coord_x_to_pix_x(0.0, width);
        c.move_to(y_axis_pos, 0.0);
        c.line_to(y_axis_pos, height);

        c.stroke();
    }

    pub fn show (&self, c: &cairo::Context, width: f64, height: f64) {
        self.draw_grid(c, width, height);

        for graph in self.graphs.clone() {
            self.draw_graph(c, width, height, graph);
        }
    }
}