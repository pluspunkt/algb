extern crate gtk;
extern crate cairo;

use std::rc::Rc;
use std::cell::RefCell;

use gtk::{prelude::*, Builder, ButtonsType, DialogFlags, MessageType, MessageDialog, Window};

mod eqn;
mod canvas;

fn main () {
    if gtk::init().is_err() {
        println!("Failed to initialize GTK.");
        return;
    }

    let builder: gtk::Builder = Builder::new();
    builder.add_from_string(include_str!("gui/gtk3/main_window.ui")).expect("Error: UI-File not found");

    let window: gtk::Window = builder.get_object("app_window").unwrap();
    let drawing_area: gtk::DrawingArea = builder.get_object("drawing_area").unwrap();
    let zoom_in_button: gtk::Button = builder.get_object("zoom_in").unwrap();
    let zoom_out_button: gtk::Button = builder.get_object("zoom_out").unwrap();
    let add_fn_button: gtk::Button = builder.get_object("add_function").unwrap();
    let example_button: gtk::Button = builder.get_object("examples").unwrap();
    let color_button: gtk::ColorButton = builder.get_object("color_picker").unwrap();
    let text_field: gtk::Entry = builder.get_object("text_field").unwrap();

    let canvas = canvas::Canvas::new(drawing_area);
    let area = canvas.area.clone();
    let canvas = Rc::new(RefCell::new(canvas));
    let canvas_for_da = canvas.clone();
    let canvas_for_zoom_out_button = canvas.clone();
    let canvas_for_zoom_in_button = canvas.clone();
    let canvas_for_example_button = canvas.clone();
    let canvas_for_add_fn_button = canvas.clone();

    zoom_out_button.connect_clicked(move |_| {
        canvas_for_zoom_out_button.borrow_mut().zoom_out();
    });

    zoom_in_button.connect_clicked(move |_| {
        canvas_for_zoom_in_button.borrow_mut().zoom_in();
    });

    add_fn_button.connect_clicked(move |_| {
        match text_field.get_text() {
            None => {
                MessageDialog::new(None::<&Window>,
                    DialogFlags::empty(),
                    MessageType::Info,
                    ButtonsType::Ok,
                    "Please enter a function").run();
            },
            Some(text) => {
                let color = color_button.get_color();

                canvas_for_add_fn_button.borrow_mut().add_graph('f', text.to_string(), 3.0,
                    color.red as f64 / 65535.0, color.green as f64 / 65535.0, color.blue as f64 / 65535.0);
            }
        }
    });

    example_button.connect_clicked(move |_| {
        canvas_for_example_button.borrow_mut().add_graph('f', "x ^ 2 - x".to_string(), 3.0, 0.0, 0.0, 1.0);
        canvas_for_example_button.borrow_mut().add_graph('g', "x ^ 5".to_string(), 3.0, 1.0, 0.0, 0.0);
        canvas_for_example_button.borrow_mut().add_graph('h', "sin ( x + 1 )".to_string(), 3.0, 0.0, 1.0, 0.0);
    });

    area.connect_draw(move |w, c| {
        canvas_for_da.borrow().show(&c, w.get_allocated_width() as f64, w.get_allocated_height() as f64); 
        Inhibit(false)
    });

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    window.show_all();
    gtk::main();
}