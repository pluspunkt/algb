#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Operator {
    Plus, Minus, Multiply, Divide, Power
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Function {
    Sin, Cosin
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Token {
    Number(f64),
    Operator(Operator),
    Function(Function),
    ParanL, ParanR,
    Variable(char)
}

impl Token {
  pub fn precedence(self) -> u8 {
    match self {
      Token::Operator(op) => { 
        match op {
            Operator::Plus | Operator::Minus => 2,
            Operator::Multiply | Operator::Divide => 3,
            Operator::Power => 4,}
      }
      _ => 0
    }
  }
}

pub fn infix_to_rpn (infix_tokens: Vec<Token>) -> Vec<Token> {
    let mut output_queue: Vec<Token> = Vec::new();
    let mut op_stack: Vec<Token> = Vec::new();

    for token in infix_tokens {
        match token {
            Token::Number(_) | Token::Variable(_) => output_queue.push(token),
            Token::Function(_) => op_stack.push(token),
            Token::Operator(_) => {
                while op_stack.len() != 0 {
                    if token.precedence() > op_stack[op_stack.len() - 1].precedence() || op_stack[op_stack.len() - 1] == Token::ParanL {
                        break;
                    }
                    output_queue.push(op_stack.pop().unwrap());
                }
                op_stack.push(token);},
            Token::ParanL => op_stack.push(Token::ParanL),
            Token::ParanR => {
                while op_stack[op_stack.len() - 1] != Token::ParanL {
                    output_queue.push(op_stack.pop().unwrap())
                }
                op_stack.pop();}
          }
    }
    output_queue.extend(op_stack.drain(..).collect::<Vec<Token>>());
    output_queue
}

fn get_token (str_token: &str) -> Token {
    match str_token.parse::<f64>() {
        Ok(n) => Token::Number(n),
        Err(..) => {
          match str_token {
            "+" => Token::Operator(Operator::Plus),
            "-" => Token::Operator(Operator::Minus),
            "*" => Token::Operator(Operator::Multiply),
            "/" => Token::Operator(Operator::Divide),
            "^" => Token::Operator(Operator::Power),
            "(" => Token::ParanL,
            ")" => Token::ParanR,
            "sin" => Token::Function(Function::Sin),
            "cos" => Token::Function(Function::Cosin),
            _ => Token::Variable(str_token.chars().next().unwrap())
          }
        }
    }
}

pub fn tokenize_str (raw_str: &str) -> Vec<Token> {
    raw_str.trim_end_matches(|c| c == ' ' || c == '\n').split(|c| c == ' ')
        .map(|x| get_token(x)).collect::<Vec<Token>>()
}

pub fn solve_rpn (equation: Vec<Token>) -> f64 {
    let mut stack: Vec<f64> = Vec::new();

    for token in equation {
        match token {
            Token::Number(n) => stack.push(n),
            Token::Function(function) => {
                let number = stack.pop().expect("Error: Expected number on stack.");
                stack.push( match function {
                    Function::Sin => number.sin(),
                    Function::Cosin => number.cos()
                });
            }
            Token::Operator(operator) => {
                let second_number = stack.pop().expect("Error: Expected number on stack.");
                let first_number = stack.pop().expect("Error: Expected number on stack.");
                stack.push( match operator {
                    Operator::Plus => first_number + second_number,
                    Operator::Minus => first_number - second_number,
                    Operator::Multiply => first_number * second_number,
                    Operator::Divide => first_number / second_number,
                    Operator::Power => first_number.powf(second_number)
                });
                }
            _ => panic!("Error: Unexpected enum variant on stack.")
        }
    }
    stack.pop().expect("Error: Result has wrong type.")
}